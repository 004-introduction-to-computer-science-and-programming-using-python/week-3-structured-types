# can have multiple pointers to the same list in memory
# changing element under one name changes it under all
# warm = [1, 2, 3]
# hot = warm
# hot is an alias for warm, changes to warm will change hot and vice versa
# 
# two lists can print the same thing but they might be independent objects
# can mutate one element of one list to check if they are dependent or independent
# 
# cloning lists will copy the elements of one list to another variable
# hot = warm[:]
# they will be different objects
# 
# sort() mutates the list and returns nothing
# sorted() does not mutate the list but returns it sorted
# 
# functions are first class objects
# has a type, can be elements of data structures, can appear in expressions, can use it as arguments 