# dictionaries
# customized index by a label
# elements are related by labels
# stores pairs of data together
# 
# my_dict = {}
# grades = {"devin":"A", "fjkdlas":"B"}
# index into a dictionary by using the key element
# order doesnt matter
# 
# grades["devin"]
# returns "A"
# no need for finding index
# grades["Kathy"] = "A"
# creates a key called kathy and pairs it with a
# 
# del(grades["fjkdlas"]) removes the value
# 
# grades.keys() returns an iterable of all of the keys, not indexable
# grades.values() returns an iterable of all of the values, not indexable
# 
# values can be anything, immutables, mutables, duplicates, other dictionaries, list, tuples
# keys must be unique and immutable types, floats can be unreliable keys
# 