# lists are ordered sequences denoted by []
# usually homogenous types but can be hetergenous
# 
# list is mutable
# can change elements
# 
# can change list elements like so
# list = [1, 3, 4]
# list[2] = 0
# list = [1, 3, 0]
# 
# can iterate through elements of list
# 
# list operations:
#
# add something to the end of a list (will add as a single element)
# my_list = [1, 3, 5, 6]
# my_list.append(9, 6)
# my_list = [1, 3, 5, 6, [9, 6]]
# 
# lists are objects and have methods and functions
# .append is a method
# 
# can add two lists together
# list1 = [1, 2]
# list2 = [2, 3]
# list3 = list1 + list2
# list3 = [1, 2, 2, 3]
# 
# extend will extend the list by however many elements needed 
# my_list = [1, 3, 5, 6]
# my_list.extend(9, 6)
# my_list = [1, 3, 5, 6, 9, 6] 
# 
# can delete using del(list[index])
# my_list = [1, 3, 5, 6]
# del(my_list[0])
# my_list = [3, 5, 6]
# 
# pop() will return the final element and mutate the list to have the final element removed
# my_list = [1, 3, 5, 6]
# my_list.pop()
# my_list = [1, 3, 5] 
# 
# remove(element) will search a list for an element and remove the first instance of it, throws error if its not found
# my_list = [1, 3, 5, 6]
# my_list.remove(6)
# my_list = [1, 3, 5]
# 
# the list() function will return a list with elements being every character in a string
# s = "abc"
# my_list = list[s]
# my_list = [a, b, c]
# 
# convert list to string using combiner.join()
# my_list = [1, 3, 5, 6]
# "".join(my_list)
# returns 1356
# 
# can use .sorted() to return a sorted version of the list, the list is not mutated
# can use .sort() to mutate the list to be sorted
# .reverse() will return reverse sort  
