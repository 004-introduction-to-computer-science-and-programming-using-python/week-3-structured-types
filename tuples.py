# tuple is a sequence with an order
# immutables
# 
# can make tuples in a few ways
# blank: tuple = ()
# with elements: tuples = (1, 2)
# 
# can concatonate using +
# tuple + (5, 7)
# tuples == (1, 2, 5, 7)
# 
# can slice tuples
# need to give commas for tuples to indicate its a tuple not a scope
# 
# can swap variables by doing
# (x, y) = (y, x)
# 
# can iterate through tuple elements 

def oddTuples(aTup):
    """
    aTup: a tuple

    returns every other element of aTup
    """
    backup = ()
    for i in range(len(aTup)):
        if(i % 2 == 0):
            backup = backup + (aTup[i], )
    return backup

print(oddTuples((6, 7, 8)))